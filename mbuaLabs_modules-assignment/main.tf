provider "aws" {
  region = "us-east-1"
}

module "computemodule" {
  source = "./compute/"
  vpc_id = module.networkingmodule.vpcid_from_networking_module
  pubsub_id = module.networkingmodule.subpub_id_from_networking_module
}

module "databasesmodule" {
  source = "./databases/"
}

module "networkingmodule" {
  source = "./networking/"
}

module "securitymodule" {
  source = "./security/"
}

output "vpcid" {
  value = module.networkingmodule.vpcid_from_networking_module
}

output "ec2_db-id" {
    value = module.computemodule.ec2_db-id
}

output "ec2_web-id" {
    value = module.computemodule.ec2_web-id
}

output "ec2-id" {
    value = module.computemodule.ec2-id
}

output "ec2_02-id" {
    value = module.computemodule.ec2_02-id
}

output "ec2_web02-id" {
    value = module.computemodule.ec2_web02-id
}

output "rds_id" {
    value = module.databasesmodule.rds_id
}

output "iam_user-ID" {
    value = module.securitymodule.iam_user-ID
}

output "iam_user-ARN" {
    value = module.securitymodule.iam_user-ARN
}