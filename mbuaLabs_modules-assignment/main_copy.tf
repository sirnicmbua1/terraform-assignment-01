/* 

provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "db" {
  ami           = var.ami
  instance_type = var.instance_type
  tags = {
    "Name" = "db_server"
  }
}

resource "aws_instance" "web" {
  ami           = var.ami
  instance_type = var.instance_type
  tags = var.webserver_tags
  depends_on = [aws_instance.db]
}


resource "aws_instance" "ec2" {
  ami           = var.ami
  instance_type = var.instance_type
}

resource "aws_eip" "elasticeip" {
  instance = aws_instance.ec2.id
}

output "EIP" {
  value = aws_eip.elasticeip.public_ip
}

resource "aws_iam_user" "myUser" {
  name = var.iam_username
}

resource "aws_iam_policy" "customPolicy" {
  name = var.iam_pol-name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:ReplaceRouteTableAssociation",
                "ec2:DeleteVpcEndpoints",
                "ec2:ResetInstanceAttribute",
                "ec2:ResetEbsDefaultKmsKeyId",
                "ec2:AttachInternetGateway",
                "ec2:ReportInstanceStatus",
                "ec2:DeleteVpnGateway",
                "ec2:CreateRoute",
                "elasticfilesystem:ClientMount",
                "glacier:PurchaseProvisionedCapacity",
                "ec2:UnassignPrivateIpAddresses",
                "ec2:CancelExportTask",
                "ec2:DeleteTransitGatewayPeeringAttachment",
                "ec2:ImportKeyPair",
                "ec2:AssociateClientVpnTargetNetwork",
                "ec2:StopInstances",
                "ec2:CreateVolume",
                "ec2:ReplaceNetworkAclAssociation",
                "ec2:CreateVpcEndpointServiceConfiguration",
                "ec2:CreateNetworkInterface",
                "ec2:CancelSpotInstanceRequests",
                "ec2:CreateTransitGatewayRoute",
                "ec2:CreateTransitGatewayVpcAttachment",
                "glacier:SetVaultNotifications",
                "glacier:CompleteMultipartUpload",
                "ec2:DeleteDhcpOptions",
                "ec2:DeleteNatGateway",
                "ec2:CancelCapacityReservation",
                "ec2:EnableTransitGatewayRouteTablePropagation",
                "glacier:ListVaults",
                "ec2:ModifyVpcEndpoint",
                "ec2:ModifyInstanceCapacityReservationAttributes",
                "ec2:CreateVpnConnection",
                "ec2:DeleteSpotDatafeedSubscription",
                "ec2:ImportVolume",
                "ec2:MoveAddressToVpc",
                "ec2:ModifyFleet",
                "ec2:RunScheduledInstances",
                "ec2:ModifyIdentityIdFormat",
                "ec2:CreateVpc",
                "ec2:RequestSpotFleet",
                "ec2:WithdrawByoipCidr",
                "ec2:ReleaseHosts",
                "ec2:DeleteTransitGatewayMulticastDomain",
                "ec2:ModifySubnetAttribute",
                "ec2:CreateSnapshot",
                "ec2:DeleteLaunchTemplateVersions",
                "ec2:DeleteNetworkAcl",
                "ec2:ModifyReservedInstances",
                "ec2:ReleaseAddress",
                "ec2:ModifyInstanceMetadataOptions",
                "ec2:AssociateDhcpOptions",
                "ec2:ModifyInstancePlacement",
                "ec2:CreateTrafficMirrorTarget",
                "ec2:ModifyTrafficMirrorFilterRule",
                "ec2:ResetFpgaImageAttribute"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "policyBind" {
  name       = var.iam_attachment
  users      = [aws_iam_user.myUser.name]
  policy_arn = aws_iam_policy.customPolicy.arn
}


resource "aws_db_instance" "myRDS" {
  name                = var.db_instance-name
  identifier          = var.db_indentifier
  instance_class      = var.db_class
  engine              = var.db_engine
  engine_version      = var.db_engVersion
  username            = var.db_username
  password            = var.db_password
  port                = var.db_port
  allocated_storage   = var.db_storage
  skip_final_snapshot = var.db_skip-snap

}

resource "aws_instance" "ec2_02" {
  ami             = var.ami
  instance_type   = var.instance_type
  security_groups = [aws_security_group.webtraffic.name]
}

resource "aws_security_group" "webtraffic" {
  name = var.allowHTTPS

  ingress {
    from_port   = var.HTTPS
    to_port     = var.HTTPS
    protocol    = var.protocol
    cidr_blocks = [var.sg_cidr]
  }

  egress {
    from_port   = var.HTTPS
    to_port     = var.HTTPS
    protocol    = var.protocol
    cidr_blocks = [var.sg_cidr]
  }
}

resource "aws_vpc" "prod-vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = var.vpc_dns-support   #gives you an internal domain name
  enable_dns_hostnames = var.vpc_dns-hostnames #gives you an internal host name
  enable_classiclink   = var.vpc_classiclink
  instance_tenancy     = var.vpc_tenancy
  tags                 = var.vpc_tags
}

resource "aws_subnet" "prod-subnet-public-1" {
  vpc_id                  = aws_vpc.prod-vpc.id
  cidr_block              = var.pubsub_cidr
  map_public_ip_on_launch = var.sub_map-public //it makes this a public subnet
  availability_zone       = var.az[0]
  tags                    = var.pubsub_tags
}

resource "aws_subnet" "prod-subnet-private-1" {
  vpc_id                  = aws_vpc.prod-vpc.id
  cidr_block              = var.privsub_cidr
  map_public_ip_on_launch = var.sub_map-public_no //it makes this a private subnet
  availability_zone       = var.az[1]
  tags                    = var.pubsub_tags
}

resource "aws_internet_gateway" "prod-igw" {
  vpc_id = aws_vpc.prod-vpc.id
  tags   = var.igw_tags
}

resource "aws_route_table" "prod-public-crt" {
  vpc_id = aws_vpc.prod-vpc.id
  route {
    //associated subnet can reach everywhere
    cidr_block = var.sg_cidr
    //CRT uses this IGW to reach internet
    gateway_id = aws_internet_gateway.prod-igw.id
  }
  tags = var.pubRT_tags
}

resource "aws_route_table_association" "prod-crta-public-subnet-1" {
  subnet_id      = aws_subnet.prod-subnet-public-1.id
  route_table_id = aws_route_table.prod-public-crt.id
}

resource "aws_security_group" "ssh-allowed" {
  vpc_id = aws_vpc.prod-vpc.id

  egress {
    from_port   = var.egress_port
    to_port     = var.egress_port
    protocol    = var.egress_protocol
    cidr_blocks = [var.sg_cidr]
  }
  ingress {
    from_port = var.SSH
    to_port   = var.SSH
    protocol  = var.protocol
    // This means, all ip address are allowed to ssh ! 
    // Do not do it in the production. 
    // Put your office or home address in it!
    cidr_blocks = [var.myIP]
  }
  //If you do not add this rule, you can not reach the NGIX  
  ingress {
    from_port   = var.HTTP
    to_port     = var.HTTP
    protocol    = var.protocol
    cidr_blocks = [var.sg_cidr]
  }
  tags = var.mySG_tags
}



resource "aws_instance" "web_02" {
  ami           = var.ami
  instance_type = var.instance_type
  count         = var.ec2_count
  tags          = var.webserver_tags
}

output "Instance_id" {
  value = aws_instance.web.id
}

*/